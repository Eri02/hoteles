# Cluster Hoteles - Almundo

## Descripción
- Se pide desarrollar el cluster de hoteles almundo, listar los hoteles, poder filtar por nombre, precio y estrellas.

![Cluster](https://github.com/Eri02/hoteles/blob/development/almundo.png)

- Debe ser responsive.
- Se hardcodea la búsqueda.
- Se linkea el componente header y footer. Pasado por Almundo.

## Tecnologías
- Angular 1.5
- Nodejs
- Se automatizan tareas con Gulp. 
  Ver [gulfile.js](https://github.com/Eri02/hoteles/blob/development/gulpfile.js) y 
  Ver su archivo de configuración [gulp.config.js](https://github.com/Eri02/hoteles/blob/development/gulp.config.js)

## Dependencias
- Revisar [package.json](https://github.com/Eri02/hoteles/blob/development/package.json)
- Revisar [bower.json](https://github.com/Eri02/hoteles/blob/development/bower.json)

## Para levantar el proyecto

- Abrir la terminal y escribir: gulp
- El proyecto corre en localhost: 8080
